import { join } from "path";
import { readFile } from "fs";
import { promisify } from "bluebird";
import { parseString } from "xml2js";
import traverse from "traverse";
import isObject from "isobject";
import isNullOrEmpty from "is-null-or-empty";
import isEmpty from "lodash.isempty";
import unique from "array-unique";
import { getMyStyle } from "./extractor-style";
import { getImageUrlByEmbedId } from "./image-mapper";

const readFileAsync = promisify(readFile);
const parseStringAsync = promisify(parseString);

const prettyPrint = obj => console.log(JSON.stringify(obj, null, 2));
const has = obj => obj && !isEmpty(obj);

const documentPathSuffix = "word/document.xml";
const mediaFolderSuffix = "word/media";

export const extractDocObj = async folderPath => {
  const documentPath = join(__dirname, folderPath, documentPathSuffix);
  const fileBuffer = await readFileAsync(documentPath);
  const documentObj = await parseStringAsync(fileBuffer);
  return documentObj;
};

export const extractBody = async documentObj => {
  return documentObj["w:document"]["w:body"][0];
};

export const extractPage = async (documentBody, pageNumber) => {};

export const extractParagraph = documentBody => {
  const paragraphs = traverse(documentBody).reduce(function(acc, paragraphObj) {
    if (this.key === "w:p") {
      for (const node of this.node) {
        const paragraphStyle = extractParagraphStyle(node);
        const pointStructure = extractPointStructure(node);
        const texts = extractText(node);
        const images = extractImages(node);

        if (has(texts) || has(images)) {
          let paragraph = {
            value: has(texts)
              ? texts.join("")
              : getImageUrlByEmbedId(images.join("")),
            style: getMyStyle(paragraphStyle, pointStructure),
            type: has(texts) ? "text" : "image"
          };
          if (!isEmpty(pointStructure)) {
            paragraph = Object.assign({}, paragraph, pointStructure);
          }
          acc.push(paragraph);
        }
      }
    }
    return acc;
  }, []);
  return paragraphs;
};

export const extractPointStructure = paragraphNode => {
  const pointStructure = traverse(paragraphNode).reduce(function(
    acc,
    pointStructureObj
  ) {
    if (this.key === "w:numPr") {
      const pointLevel = this.node[0]["w:ilvl"][0]["$"]["w:val"];
      acc["pointLevel"] = pointLevel;
    }
    return acc;
  },
  {});
  return pointStructure;
};

export const extractParagraphStyle = paragraphNode => {
  const paragraphStyles = traverse(paragraphNode).reduce(function(
    acc,
    paragraphStyleObj
  ) {
    if (this.key === "w:pStyle") {
      const style = this.node[0]["$"]["w:val"];
      if (style) {
        acc.push(style);
      }
    }
    return acc;
  },
  []);
  return paragraphStyles.join("");
};

export const extractText = documentBody => {
  const texts = traverse(documentBody).reduce(function(acc, textObj) {
    if (this.key === "w:t") {
      let text = textObj[0];
      if (!text) {
        return;
      }

      if (isObject(text)) {
        if (text.hasOwnProperty("_")) {
          acc.push(text["_"]);
        }
      } else {
        acc.push(text);
      }
    }
    return acc;
  }, []);
  return texts;
};

export const extractImages = paragraphNode => {
  const embed = traverse(paragraphNode).reduce(function(acc, embedObj) {
    if (this.key === "r:embed") {
      acc.push(this.node);
    }
    return acc;
  }, []);
  return embed;
};

export const extractTable = documentObj => {};
