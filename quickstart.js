const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const OAuth2Client = google.auth.OAuth2;
const SCOPES = ['https://www.googleapis.com/auth/presentations'];
const TOKEN_PATH = 'credentials.json';
const PAGE_TYPE = ['SLIDE', 'MASTER', 'LAYOUT', 'NOTES', 'NOTES_MASTER'];

export const startGenerate = async () => {
  // Load client secrets from a local file.
  fs.readFile('client_secret.json', async (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Slides API.
    const result = await authorize(JSON.parse(content), genSlides);
    console.log(result)
    return result;
  });
}

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
async function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new OAuth2Client(client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, async (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    const result = await callback(oAuth2Client);
    return result;
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return callback(err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

function setupPages() {
  const WORD = 'output.json';
  return new Promise((resolve) => fs.readFile(WORD, 'utf8', (err, data) => {
    if (err) return console.log("setup pages: ", err)
     resolve(JSON.parse(data));
  }))
}

function renderSlide(req, data) {

  Object.keys(data).map((key) => {
    const slideId = 'ABCDEF'+key;
    let createSlide = {
      'objectId': '',
      'slideLayoutReference': {
        'predefinedLayout': ''
      },
      'placeholderIdMappings': []
    };
    let others = [];
    let predefinedLayout = '';
    createSlide = Object.assign({}, createSlide, {'objectId': slideId });
    if (!data[key].body) {
      predefinedLayout = 'TITLE';
      createSlide = Object.assign({},
        createSlide, {
        'slideLayoutReference': {
          'predefinedLayout': predefinedLayout
        },
        'placeholderIdMappings': [
          {
            'layoutPlaceholder': {
              'type': 'TITLE',
              'index': 0
            },
            'objectId': 'title_'+key
          }
        ]
      }
      )
      return req.push({'createSlide': createSlide}, {
        'insertText': {
          'objectId': 'a'+key+'_title',
          'text': data[key].title.value
        }
      })
    } else {
      let body = data[key].body;
      let type = data[key].type;
      let field = Object.keys(data[key].body);
      if (type === 'note') return;
      let predefinedLayout = '';
      let arr = [];
      let temp = {
        'slideLayoutReference': {
            'predefinedLayout': ''
          },
        'placeholderIdMappings': createSlide.placeholderIdMappings
      };
      temp.placeholderIdMappings.push({
        'layoutPlaceholder': {
          'type': 'TITLE',
          'index': 0
        },
        'objectId': 'title_'+key
      })
      createSlide = Object.assign({}, createSlide, temp)
      if (field.indexOf('passage')!==-1) {
        predefinedLayout = 'TITLE_AND_BODY';
        temp = {
          'slideLayoutReference': {
            'predefinedLayout': predefinedLayout
          },
          'placeholderIdMappings': createSlide.placeholderIdMappings
        }
        temp.placeholderIdMappings.push({
          'layoutPlaceholder': {
          'type': 'BODY',
          'index': 0
        },
        'objectId': 'body_'+key
        })
        createSlide = Object.assign({}, createSlide, temp)
        arr.push({
          'insertText': {
            'objectId': 'body_'+key,
            'text': body.passage.value
          }
        })
      }
      if (field.indexOf('points')!==-1) {
        predefinedLayout = 'TITLE_AND_BODY';
        temp = {
          'slideLayoutReference': {
            'predefinedLayout': predefinedLayout
          },
          'placeholderIdMappings': createSlide.placeholderIdMappings
        }
        temp.placeholderIdMappings.push({
          'layoutPlaceholder': {
          'type': 'BODY',
          'index': 0
        },
        'objectId': 'point_'+key
        })
        createSlide = Object.assign({}, createSlide, temp)
        arr.push({
          'insertText': {
            'objectId': 'point_'+key,
            'text': Object.values(body.points).map(m=>m.value).join('\n')
          }
        },
        {
          'createParagraphBullets': {
            'objectId': 'point_'+key,
            'bulletPreset': 'BULLET_ARROW_DIAMOND_DISC',
            'textRange': {
              'type': 'ALL'
            }
          }
        })
      }
      if (field.indexOf('image')!==-1) {
        predefinedLayout = 'TITLE_AND_TWO_COLUMNS';
        temp = {
          'slideLayoutReference': {
            'predefinedLayout': predefinedLayout
          },
          'placeholderIdMappings': createSlide.placeholderIdMappings
        }
        temp.placeholderIdMappings.push({
          'layoutPlaceholder': {
          'type': 'PICTURE',
          'index': 0
        },
        'objectId': 'picture_'+key
        })
        createSlide = Object.assign({}, createSlide, temp)
        arr.push({
          'createImage': {
            'objectId': 'image_'+key,
            'url': body.image.value
          }
        })
      }
      req.push({'createSlide': createSlide}, ...arr);
      return req;
    }
  })
}

/**
 * Prints the number of slides and elements in a sample presentation:
 * https://docs.google.com/presentation/d/1EAYk18WDjIG-zp_0vLm3CsfQh_i8eXc67Jo2O9C6Vuc/edit
 * @param {OAuth2Client} auth The authenticated Google OAuth client.
 */
async function genSlides (auth) {
  let slides = google.slides({version: 'v1', auth});
  let pages = await setupPages();
  let temp = Object.assign({}, pages.slides);
  let x = [];
  const pid = '1gJA3z5G3vD9CgV5s9qcqJ-aGhn-Kr7_FR5Iq1XvJwh4';
  await renderSlide(x, temp);
  //console.log("x: ", JSON.stringify(x));
  await slides.presentations.get({
    'presentationId': pid,
    'fields': 'slides.objectId'
  }, (err, res) => {
    if (err) return console.log("error: " + err);
    
    slides.presentations.batchUpdate({
      presentationId: pid,
      resource: {
        requests: x
      }
    }, (err, res) => {
      if (err) console.log(err);
      return {'link': 'https://docs.google.com/presentation/d/'+res.data.presentationId+'/edit'}
    })
    
  })
}
