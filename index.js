import { extractDocObj, extractBody, extractParagraph } from "./extractor";
import { startGenerate } from "./quickstart";
import { writeFile, readFile } from "fs";
import { promisify } from "bluebird";
import { spawn } from 'child_process';
import express from 'express';
import multer from 'multer';
import path from 'path';
import unzipper from 'decompress-zip';
import rimraf from 'rimraf';
const upload = multer({ dest: 'uploads/' })
const app = express()
const writeFileAsync = promisify(writeFile);

app.post('/slides', upload.single('document'), createSlides)

app.listen(3000, () => console.log('Example app listening on port 3000!'))

async function createSlides(req, res) {
  console.log(req.file)
  let filepath = path.join(req.file.destination, req.file.filename)
  let zip = new unzipper(filepath)
  const docPath = path.join(__dirname, '/test-words/test')
  rimraf(docPath, async () => {
    console.log("cleaning");
    zip.on('extract', async()=>{
      console.log("extracting")
      const documentObj = await extractDocObj("test-words/test");
      const documentBody = await extractBody(documentObj);
      const paragraphs = extractParagraph(documentBody);
      const output = {
        paragraphs
      };
      await writeFileAsync("output/v1.2.json", JSON.stringify(output, null, 2));
      const py = spawn('python', ['./summary.py'])
      let dataString = '';
      py.stdout.on('data', function(data){
        dataString += data.toString();
      });
      py.stdout.on('end', async () => {
        const obj = JSON.parse(dataString)
        //res.json(obj)
        const result = await startGenerate();
        res.json(result)
      });
    })
    zip.extract({path: docPath})
  })
}
