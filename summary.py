#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals

import json
import copy
from collections import OrderedDict
from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words


LANGUAGE = "english"
SENTENCES_COUNT = 4


def process_slides(json_str_slides):
    tokenizer = Tokenizer(LANGUAGE)
    stemmer = Stemmer(LANGUAGE)
    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
    json_obj = json.loads(json_str_slides)
    slides = transform_json(json_obj)
    resp = []
    for key, slide in slides.iteritems():
        slide = summary(slide, tokenizer, summarizer)
        if need_split_slide(slide):
            s1, s2 = split_slide(slide)
            s1['type'] = 'page'
            s2['type'] = 'note'
            resp.append(s1)
            resp.append(s2)
        else:
            slide['type'] = 'page'
            resp.append(slide)
    resp_dict = OrderedDict({i: resp[i] for i in xrange(len(resp))})

    return {"slides": resp_dict}


def transform_json(json_obj):
    slides = to_slides(json_obj)
    resp = {}
    for i in xrange(len(slides)):
        slide = slides[i]
        if 'body' in slide and 'points' in slide['body']:
            points = slide['body']['points']
            new_points = OrderedDict({j: points[j] for j in xrange(len(points))})
            slide['body']['points'] = new_points
        resp[i] = slide
    return OrderedDict(resp)


def to_slides(json_obj):
    slides = []
    slide = {}
    body = {}
    for value in json_obj['paragraphs']:
        if 'title' in slide and value['style'] == 'passage':
            if 'passage' in body:
                body['passage']['value'] += value['value']
            else:
                body['passage'] = {}
                # print (value)
                body['passage']['value'] = value['value'] if 'value' in value else ""
        elif value['style'] == 'passage':
            if 'points' not in body:
                body['points'] = []
            body['points'].append({"value": value['value']})
        elif value['style'] == 'title':
            if body:
                body1 = copy.deepcopy(body)
                slide1 = copy.deepcopy(slide)
                slide1['body'] = body1
                slides.append(slide1)  # save the  previous result first
            body = {}
            slide['title'] = {}
            slide['title']['value'] = value['value']
        elif value['style'] == 'points':
            if 'points' not in body:
                body['points'] = []
            body['points'].append({'value': value['value']})
    slide['body'] = body
    slides.append(slide)
    return slides



def split_slide(slide):
    slide_1 = copy.deepcopy(slide)
    del slide_1['body']['points']
    del slide['body']['passage']
    return (slide, slide_1)


def need_split_slide(slide):
    return 'body' in slide and 'passage' in slide['body'] and 'points' in slide['body']


def summary(slide, tokenizer, summarizer):
    if "body" in slide:
        # print ("body in slide")
        if 'passage' in slide['body']:
            parser = PlaintextParser.from_string(slide['body']['passage']['value'], tokenizer)
            shorten_passage = {}
            i = 0
            for sentence in summarizer(parser.document, SENTENCES_COUNT):
                shorten_passage['{}'.format(i)] = {"value": str(sentence)}
                i += 1
            if 'points' not in slide['body'] or not slide['body']['points']:
                slide['body']['points'] = shorten_passage
    return slide


def test_parser():
    url = "https://www.lipsum.com/"
    content = """
    Headings are 14 points, flush left, and boldfaced. Use initial capitals. A good typeface for the heading is Arial, because it holds bold facing well. To preserve hierarchy, allot three line skips before the heading and two line skips after. At least one paragraph should follow a heading before a subheading exists. The typeface given here for the text portion of this report is Times New Roman (on a Macintosh, Times would a comparable choice). Book Antiqua would also be a professional choice, especially for the single column format of a thesis. On a Macintosh, comparable typefaces to Book Antiqua are New Century Schoolbook and Palatino. In industry, the recommended spacing for a report is single spacing.
    """
    parser = PlaintextParser.from_string(content, Tokenizer(LANGUAGE))
    # or for plain text files
    # parser = PlaintextParser.from_file("document.txt", Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)

    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)

    for sentence in summarizer(parser.document, SENTENCES_COUNT):
        print(sentence)


def main():
    with open('./output/v1.2.json', 'r') as myfile, open('output.json', 'w') as out:
        data = json.dumps(json.load(myfile))
        data = process_slides(data)
        json.dump(data, out)
        print (json.dumps(data))
    return json.dumps(data)


if __name__ == "__main__":
    main()
