import isEmpty from "lodash.isempty";

const titleRegex = new RegExp("^Heading");
const pointsRegex = new RegExp("^ListParagraph");

export const getMyStyle = (paragraphStyle, pointStructure) => {
  if (titleRegex.test(paragraphStyle)) {
    return "title";
  }
  if (
    pointsRegex.test(paragraphStyle) ||
    (pointStructure && !isEmpty(pointStructure))
  ) {
    return "points";
  }
  return "passage";
};
