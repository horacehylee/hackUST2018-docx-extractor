const imageMap = {
  rId10: "https://picsum.photos/200/300/?random",
  rId11: "https://picsum.photos/200/300/?random",
  rId12: "https://picsum.photos/200/300/?random",
  rId13: "https://picsum.photos/200/300/?random",
  rId14: "https://picsum.photos/200/300/?random",
  rId15: "https://picsum.photos/200/300/?random"
};

export const getImageUrlByEmbedId = embedId => {
  return imageMap[embedId];
};
