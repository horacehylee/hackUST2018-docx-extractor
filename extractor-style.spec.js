import { getMyStyle } from "./extractor-style";

describe("extractor-style", () => {
  describe("getMyStyle", () => {
    it.only("should return title if start with Heading", () => {
      const style = getMyStyle("Heading2");
      expect(style).toEqual("title");
    });

    it("should return passage if is ListParagraph", () => {
      const style = getMyStyle("ListParagraph");
      expect(style).toEqual("passage");
    });

    it("should return passage if is TOCHeading", () => {
      const style = getMyStyle("TOCHeading");
      expect(style).toEqual("passage");
    });

    it("should return passage if is null", () => {
      const style = getMyStyle("");
      expect(style).toEqual("passage");
    });

    it("should return points if it is ListParagraph", () => {
      const style = getMyStyle("ListParagraph");
      expect(style).toEqual("points");
    });
  });
});
