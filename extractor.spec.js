import {
  extractDocObj,
  extractText,
  extractBody,
  extractPage,
  extractParagraph,
  getMyStyle
} from "./extractor";
import { join } from "path";
import { writeFile } from "fs";
import { promisify } from "bluebird";

const writeFileAsync = promisify(writeFile);

describe("Extractor", () => {
  describe("extract", () => {
    it("should return document obj", async () => {
      const documentObj = await extractDocObj("test-words/hard");
      expect(documentObj).not.toBeNull();
      expect(documentObj).toMatchSnapshot();

      await writeFileAsync(
        join(__dirname, "sample/documentObj.json"),
        JSON.stringify(documentObj, null, 2)
      );
    });
  });

  describe("extract body", () => {
    it("should return body object", async () => {
      const documentObj = await extractDocObj("test-words/hard");
      const documentBody = await extractBody(documentObj);
      expect(documentBody).not.toBeNull();
      expect(documentBody).toMatchSnapshot();
    });
  });

  describe("extract text", () => {
    it("should return text from page 1", async () => {
      const documentObj = await extractDocObj("test-words/hard");
      const documentBody = await extractBody(documentObj);
      const text = extractText(documentBody);
      expect(text).not.toBeNull();
      expect(text).toMatchSnapshot();
    });
  });

  describe.only("extract paragraph", () => {
    it("should return paragraph", async () => {
      const documentObj = await extractDocObj("test-words/hard");
      const documentBody = await extractBody(documentObj);
      const paragraphs = extractParagraph(documentBody);
      expect(paragraphs).not.toBeNull();
      expect(paragraphs).toMatchSnapshot();
    });
  });
});
