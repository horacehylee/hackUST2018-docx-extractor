FROM node:9

RUN apt-get update

RUN  apt-get install -y python python-dev python-pip python-virtualenv && \
      rm -rf /var/lib/apt/lists/*

RUN mkdir app

COPY ./requirements.txt app/requirements.txt

COPY ./package.json app/package.json

WORKDIR app/

RUN pip install -r ./requirements.txt

RUN npm install

RUN python -c "import nltk; nltk.download('punkt')"

COPY ./ ./

ENTRYPOINT npm run start
